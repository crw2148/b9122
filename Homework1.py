# Question 1
# I used the replace function to replace all
# instances of the letter (i.e., string) 'a'
# with the letter (i.e., string) 'i'.
def replacelettera(word):
    if not type(word) is str:
        print("That isn't a string variable! Try again.")
    else:
        word = word.replace('a', 'i')
        return word


# Question 2
# Given an integer greater than 1, we begin
# from two integers a and b set equal to 1
# and 1. While a and b are both less than or
# equal to the integer argument, we print their
# multiple, looping first over b and adding 1 after
# each loop and then re-initializing the loop by
# adding 1 to and re-setting b equal to 1. This
# produces the sequence of outputs shown in the
# homework assignment.
def factormult(n):
    if not (type(n) is int and n > 1):
        print("n must be an integer greater than 1! Try again.")
    else:
        a, b = 1, 1
        while a <= n:
            while b <= n:
                print(a,"*",b,"=",a*b)
                b = b + 1
            a, b = a + 1, 1


# Question 3
# After splitting the string argument by spaces and
# sorting the resulting list variable by length,
# I create an empty list and loop over all of the
# words in the sorted variable, adding the length
# of each word to the list. Finally, I zip the
# two lists containing word length and the sorted
# word order.
def wordlength(sentence):
    if not type(sentence) is str:
        print("That isn't a string variable! Try again.")
    else:
        sentence = list(sentence.split())
        sentence = sorted(sentence, key=len)

        lengths = list()
        for x in sentence:
            lengths.append(len(x))

        return list(zip(lengths, sentence))

# Question 4
# This function takes some filename containing text
# as its first argument and an integer argument
# describing the number of most frequent words to
# be printed as its second argument. After identifying
# words by splitting the file text by spaces, I create
# an empty dictionary and loop over all of the words in
# the file, adding the word to the dictionary if it
# doesn't already belong and adding 1 to its count if
# it does. Finally, I sort the items of the dictionary
# by their length and then print the words in
# decreasing order of frequency in the format shown
# in the homework assignment.
def wordfreqcounter(filename, head):
    if not (type(head) is int and head > 0):
        print("The head argument must be an integer greater than 0! Try again.")
    else:
        fh = open(filename)
        txt = fh.read()
        txt = txt.lower()

        words = txt.split()
        dic = dict()
        for x in words:
            if x in dic:
                dic[x] = dic[x] + 1
            else:
                dic[x] = 1
        dic = sorted(dic.items(), key=lambda x: x[1])
        for x in range(1, head + 1):
            print(dic[-x][1], dic[-x][0], '\n')


# Question 5
import re

# Part a)
# I use findall to identify all strings that
# match either XXXX-XX-XX, where X is a digit
# (i.e., between 0 and 9). I then loop over any
# resulting matches and attempt to format them
# as a date with format %Y-%m-%d using datetime.
# If the match cannot be formatted (i.e., if the
# match is a numeric string that can't represent
# a real date), the error is caught. All surviving
# (i.e., real date) matches are returned.
import datetime
def finddates(x):
    match = re.findall(r'\d{4}-\d{2}-\d{2}', x)

    datematch = list()
    for x in match:
        try:
            datetime.datetime.strptime(x, '%Y-%m-%d').date()
            datematch.append(x)
        except ValueError:
            print("Found a matching string that's not a real date! Moving on...")

    return datematch


# Part b)
# I use findall to identify all strings that
# match either XXXXX-XXXX or XXXXX, where X
# is a digit (i.e., between 0 and 9).
def findzipcodes(x):
    return re.findall(r'\d^{5}-\d^{4}|\d^{5}', x)


# Part c)
# I use findall to identify all strings that
# match local@domain.com, where the email must
# end in ".com" and not another extension (per
# the homework assignment).
def findemails(x):
    return re.findall(r'\w+@\w+\.com', x)


# Question 6
# Given 2 numpy arrays, the indices of
# the arrays are compared both row-wise
# and column-wise, unless the arrays are
# row vectors, in which case they are only
# compared column-wise. Matching indices/
# pairs of indices are printed.
import numpy as np
def indexmatch(a, b):
    if not (type(a) == np.ndarray and type(b) == np.ndarray):
        print("This function is only for numpy arrays! Try again.")
    elif not (a.shape == b.shape):
        print("The arrays are different sizes! Try again.")
    else:
        indices = list()
        for i in range(0, a.shape[0]):
            if len(a.shape) > 1:
                for j in range(0, a.shape[1]):
                    if a[i, j] == b[i, j]:
                        indices.append([i, j])
            else:
                if a[i] == b[i]:
                    indices.append(i)
        print(indices)

